#include <linux/build-salt.h>
#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

BUILD_SALT;

MODULE_INFO(vermagic, VERMAGIC_STRING);
MODULE_INFO(name, KBUILD_MODNAME);

__visible struct module __this_module
__section(.gnu.linkonce.this_module) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

#ifdef CONFIG_RETPOLINE
MODULE_INFO(retpoline, "Y");
#endif

static const struct modversion_info ____versions[]
__used __section(__versions) = {
	{ 0xfc47fcb2, "module_layout" },
	{ 0x37a0cba, "kfree" },
	{ 0xfd7ad52a, "kmalloc_caches" },
	{ 0x3fe1ec28, "kobject_put" },
	{ 0xa0ef141e, "kobject_add" },
	{ 0xfad1c85c, "kobject_init" },
	{ 0xca43f22b, "kmem_cache_alloc_trace" },
	{ 0x314b20c8, "scnprintf" },
	{ 0x8be0fddd, "sysfs_notify" },
	{ 0xe2d5255a, "strcmp" },
	{ 0xc5850110, "printk" },
	{ 0xbcab6ee6, "sscanf" },
	{ 0xb1ad28e0, "__gnu_mcount_nc" },
};

MODULE_INFO(depends, "");


MODULE_INFO(srcversion, "58413AF1797EA6C0AC4D11C");
