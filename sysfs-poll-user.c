#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h> 
#include <sys/stat.h> 
#include <poll.h>

#define TEST_SYSFS_TRIGGER  "/sys/hello/trigger"

int main(int argc, char **argv)
{
	int cnt, trigger_fd, rv;
	char attrData[100];
	struct pollfd ufds;

	while(1) {
		if ((trigger_fd = open(TEST_SYSFS_TRIGGER, O_RDWR)) < 0) {
			perror("Unable to open trigger");
			continue;
		}

		if((lseek(trigger_fd,0L,SEEK_SET)) <0) {
			perror("Failed to set pointer\n");
			close( trigger_fd );
			continue;
		}

		cnt = read( trigger_fd, attrData, 100 );
		ufds.revents = 0;

		ufds.fd = trigger_fd;
		ufds.events = POLLPRI|POLLERR;

		if (( rv = poll( &ufds, 1, 1000000)) < 0 )
			perror("poll error");
		else if (rv == 0)
			printf("Timeout occurred!\n");
		else {
			printf("triggered\n");
			cnt = read( trigger_fd, attrData, 100 );
		}

		printf( "revents[0]: %08X\n", ufds.revents );
		close( trigger_fd );
	}

	return 0;
}
